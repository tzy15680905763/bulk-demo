# bulk-demo

## 项目说明

本项目是一些示例的合集，每一个都是独立的项目

主要做一些中间件和 Spring Boot 的整合预计实际应用示例

更多信息文档参考 https://doc.bulkall.top/

后续会慢慢补充

## 目录简介

| 项目                                                                                                     | 描述                                                                                   |
|--------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------|
| [spring-boot-bloom-filter](./spring-boot-bloom-filter/README.md)                                       | Spring Boot 整合使用布隆过滤器演示                                                              |
| [spring-boot-websocket](./spring-boot-websocket/README.md)                                             | Spring Boot 整合使用 Socket & 在线聊天演示                                                     |
| [spring-boot-lock](./spring-boot-lock)                                                                 | Spring Boot 整合各种分布式锁演示                                                               |
| [spring-boot-lock-zookeeper](./spring-boot-lock/spring-boot-lock-zookeeper/README.md)                  | Spring Boot 整合 Zookeeper 分布式锁演示                                                      |
| [spring-boot-lock-redis](./spring-boot-lock/spring-boot-lock-redis/README.md)                          | Spring Boot 整合 Redis 分布式锁演示                                                          |
| [spring-boot-lock-mysql](./spring-boot-lock/spring-boot-lock-mysql/README.md)                          | Spring Boot 基于 MySQL 实现分布式锁（乐观锁，悲观锁）演示                                               |
| [spring-boot-mq](./spring-boot-mq)                                                                     | Spring Boot 中各种 MQ 使用演示                                                              |
| [spring-boot-mq-redis](./spring-boot-mq/spring-boot-mq-redis/README.md)                                | Spring Boot 基于 Redis 的 list、pub/sub、stream 三种方式实现 MQ 演示                              |
| [spring-boot-mq-rabbit](./spring-boot-mq/spring-boot-mq-rabbit/spring-boot-mq-rabbit-basics/README.md) | Spring Boot 集成 RabbitMQ , 多种类型队列使用、消费者 ack、生产者确认、事务、并发消费、顺序消费（消费者单活模式）、批量发送、批量消费等等演示 |
| [spring-boot-mq-kafka](./spring-boot-mq/spring-boot-mq-kafka/spring-boot-mq-kafka-basics/README.md)    | Spring Boot 集成 Kafka , 基础队列使用、异步发送、同步发送、消费者 ack、事务、并发消费、顺序消费、批量发送、批量消费等等演示           |
| [spring-boot-elasticsearch](./spring-boot-search/spring-boot-elasticsearch/README.md)                  | Spring Boot 集成 Elasticsearch 演示                                                      |
| [spring-boot-swagger3](./spring-boot-doc/spring-boot-swagger3/README.md)                               | Spring Boot 集成 Swagger3 演示                                                           |
| [spring-boot-knife4j](./spring-boot-doc/spring-boot-knife4j/README.md)                                 | Spring Boot 集成 Knife4j 演示                                                            |
| [spring-boot-validation](./spring-boot-validation/README.md)                                           | Spring Boot 集成 validation 实现优雅的数据校验，包含分组校验、嵌套校验、自定义校验注解演示                            |
| [spring-boot-unit-test](./spring-boot-test/spring-boot-unit-test/README.md)                            | Spring Boot 单元测试演示，以及 Mockito 使用,模拟了 controller、service、mapper，模拟数据库防止污染数据           |

## 后续计划

- [x] Spring Boot 集成 RabbitMQ 各种功能点演示
- [x] Spring Boot 集成 ES
- [ ] Spring Boot 集成 ShardingSphere 分库分表
- [ ] Spring Boot 集成 监控
- [ ] Spring Boot 集成 Quartz
- [ ] Spring Boot 集成 邮件发送
- [ ] Spring Boot 集成 cache
- [ ] 其他...
