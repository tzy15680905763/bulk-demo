package top.bulk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 散装java
 * @version 1.0.0
 * @date 2024-04-02
 */
@SpringBootApplication
public class SpringBootKnife4jApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootKnife4jApplication.class, args);
        System.out.println("访问原生swagger-ui http://127.0.0.1:8080/swagger-ui.html ");
        System.out.println("访问Knife4j http://127.0.0.1:8080/doc.html ");
    }
}