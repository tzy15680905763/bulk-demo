package top.bulk.extension;

import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.OpenAPIService;
import org.springdoc.core.customizers.OpenApiBuilderCustomizer;
import org.springframework.stereotype.Component;

/**
 * @author 散装java
 * @version 1.0.0
 * @date 2024-04-02
 */
@Slf4j
@Component
public class GlobalApiServiceCustomer implements OpenApiBuilderCustomizer {

    @Override
    public void customise(OpenAPIService openApiService) {

    }
}
