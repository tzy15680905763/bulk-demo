package top.bulk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 散装java
 * @version 1.0.0
 * @date 2024-04-02
 */
@SpringBootApplication
public class SpringBootOpenapiApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootOpenapiApplication.class, args);
        System.out.println("访问 http://127.0.0.1:8080/test 会重定向到swagger页面");
    }
}