package top.bulk.controller.admin;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import top.bulk.entity.CommonResult;
import top.bulk.entity.User;

/**
 * @author 散装java
 * @version 1.0.0
 * @date 2024-04-02
 */
@Tag(name = "AdminControllerApi", description = "管理员相关接口")
public interface AdminControllerApi {

    @Operation(summary = "管理员添加用户",
            description = "根据姓名添加用户并返回",
            parameters = {
                    @Parameter(name = "name", description = "姓名")
            },
            responses = {
                    @ApiResponse(description = "返回添加的用户", content = @Content(mediaType = "application/json", schema = @Schema(anyOf = {CommonResult.class, User.class}))),
                    @ApiResponse(responseCode = "400", description = "返回400时候错误的原因")
            }
    )
    CommonResult<User> addUser(String name);

    @Operation(summary = "管理员删除用户", description = "根据姓名删除用户")
    @ApiResponse(description = "返回添加的用户", content = @Content(mediaType = "application/json", schema = @Schema(implementation = User.class)))
    CommonResult<User> delUser(@Parameter(description = "姓名") String name);


    @Operation(summary = "管理员更新用户", description = "管理员根据姓名更新用户")
    @ApiResponse(description = "返回更新的用户", content = @Content(mediaType = "application/json", schema = @Schema(implementation = User.class)))
    CommonResult<User> updateUser(@Parameter(
            schema = @Schema(implementation = User.class),
            required = true, description = "用户类") User user);
}