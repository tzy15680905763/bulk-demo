package top.bulk.controller.user;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import top.bulk.entity.User;

/**
 * @author 散装java
 * @version 1.0.0
 * @date 2024-04-02
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController implements UserControllerApi {

    @PostMapping("/add/{name}")
    @Override
    public User addUser(@PathVariable String name) {
        return new User(name, 18);
    }

    @GetMapping("/del/{name}")
    @Override
    public void delUser(@PathVariable String name) {
        log.info("删除name={}的用户", name);
    }
}
