package top.bulk.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 散装java
 * @version 1.0.0
 * @date 2024-04-02
 */
@Schema(name = "User", description = "用户信息")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Schema(name = "name", description = "姓名")
    private String name;
    @Schema(name = "age", description = "年龄")
    private int age;
}
