package top.bulk.search.es.repository;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.xcontent.XContentFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import top.bulk.search.es.entity.EsProductEntity;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @author 散装java
 * @date 2023-07-11
 */
@SpringBootTest
@Slf4j
class ProductRepositoryTest {
    @Autowired
    ProductRepository productRepository;

    @Autowired
    RestHighLevelClient restHighLevelClient;

    // 索引名称, 这里的示例都是用的这个索引
    private static final String INDEX = "product_index";

    /**
     * 测试新增
     */
    @Test
    public void insert() {
        EsProductEntity data = new EsProductEntity();
        data.setId(1);
        data.setTitle("标题1");
        data.setDescription("描述1");
        data.setCreatedTime(LocalDateTime.now());
        productRepository.save(data);
        log.info("插入成功");
    }

    @Test
    public void query() {
        Optional<EsProductEntity> entity = productRepository.findById(1);
        log.info("查询成功：{}", entity.toString());
    }


    /**
     * 更新一条记录
     * 这里要注意，如果使用 save 方法来更新的话，必须是全量字段，否则其它字段会被覆盖。
     */
    @Test
    public void updateBySave() {
        EsProductEntity data = new EsProductEntity();
        data.setId(1);
        data.setDescription("使用save更新");
        productRepository.save(data);
        // 如此写法，更新完成之后呢，title 字段是没有值的是，null 。 因此，如果使用此种方式，需要填充所有字段
        log.info("更新成功");
    }

    /**
     * 删除
     */
    @Test
    public void delete() {
        Integer id = 1;
        productRepository.deleteById(id);
        log.info("id: {} 删除成功", id);
    }

    /**
     * restHighLevelClient 更新，单字段更新
     */
    @SneakyThrows
    @Test
    public void update() {
        // 要更新的文档所在的索引名称
        String index = "product_index";
        // 要更新的文档的唯一标识符
        String documentId = "1";
        // 要更新的字段名称
        String fieldName = "description";
        // 要更新字段的新值
        String newValue = "更新后的描述字段";
        // 更新请求对象，用于描述要执行的更新操作
        UpdateRequest updateRequest = new UpdateRequest(index, documentId)
                .doc(XContentFactory.jsonBuilder()
                        .startObject()
                        .field(fieldName, newValue)
                        .endObject());

        UpdateResponse updateResponse = restHighLevelClient.update(updateRequest, RequestOptions.DEFAULT);
        log.info("索引{}，id{}，更新成功：{}", index, documentId, updateResponse.toString());
    }

    /**
     * restHighLevelClient 模糊查询，高亮查询
     */
    @SneakyThrows
    @Test
    public void queryByClient() {
        SearchRequest searchRequest = new SearchRequest(INDEX);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchQuery("title", "标题"));
        searchRequest.source(searchSourceBuilder);

        // 设置高亮显示
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.field("title"); // 高亮显示的字段
        highlightBuilder.preTags("<em>"); // 高亮显示的起始标签
        highlightBuilder.postTags("</em>"); // 高亮显示的结束标签
        searchSourceBuilder.highlighter(highlightBuilder);

        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        log.info("client 查询: {}", searchResponse.toString());
        // 输出如下
        // {"took":78,"timed_out":false,"_shards":{"total":1,"successful":1,"skipped":0,"failed":0},"hits":{"total":{"value":1,"relation":"eq"},"max_score":0.2876821,"hits":[{"_index":"product_index","_type":"_doc","_id":"1","_score":0.2876821,"_source":{"_class":"top.bulk.search.es.entity.EsProductEntity","id":1,"title":"标题1","description":"描述1","createdTime":"2023-07-17T10:35:19"},"highlight":{"title":["<em>标题</em>1"]}}]}}
    }
}