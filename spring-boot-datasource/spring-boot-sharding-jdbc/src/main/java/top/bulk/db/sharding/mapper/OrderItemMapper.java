package top.bulk.db.sharding.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.bulk.db.sharding.entity.OrderItem;

import java.util.List;

/**
 * 订单明细 mapper
 *
 * @author 散装java
 * @date 2023-08-29
 */
@Mapper
public interface OrderItemMapper {
    /**
     * 根据id查询订单明细
     *
     * @param id 订单id
     * @return 订单详情
     */
    OrderItem selectById(@Param("id") Long id);

    /**
     * 根据 orderId 查询所有订单
     *
     * @param orderId 用户id
     * @return 订单列表
     */
    List<OrderItem> selectListByUserId(@Param("order_id") Long orderId);

    /**
     * 插入一条订单明细数据
     *
     * @param orderItem 订单明细
     */
    void insert(OrderItem orderItem);
}
