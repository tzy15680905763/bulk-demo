package top.bulk.db.sharding.entity;

import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;

/**
 * 订单主表
 *
 * @author 散装java
 * @date 2023-08-29
 */
@Data
@ToString
public class Order {
    private Long id;
    private Long userId;
    private LocalDateTime addTime;
}
