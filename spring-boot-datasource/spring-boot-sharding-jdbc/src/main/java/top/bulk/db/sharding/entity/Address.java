package top.bulk.db.sharding.entity;

import lombok.Data;

/**
 * 地址表
 * 演示 sharding 广播
 *
 * @author 散装java
 * @date 2023-08-31
 */
@Data
public class Address {
    private Long id;
    private Long userId;
    private String province;
    private String city;
    private String region;
    private String detail;
}
