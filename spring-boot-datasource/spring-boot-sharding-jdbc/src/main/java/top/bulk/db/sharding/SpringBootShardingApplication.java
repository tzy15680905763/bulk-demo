package top.bulk.db.sharding;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author 散装java
 * @date 2023-08-29
 */
@SpringBootApplication
@MapperScan(basePackages = "top.bulk.db.sharding.mapper")
public class SpringBootShardingApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootShardingApplication.class, args);
    }
}
