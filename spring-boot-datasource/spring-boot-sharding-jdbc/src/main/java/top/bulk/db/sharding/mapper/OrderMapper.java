package top.bulk.db.sharding.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.bulk.db.sharding.entity.Order;

import java.util.List;

/**
 * 订单 mapper
 *
 * @author 散装java
 * @date 2023-08-29
 */
@Mapper
public interface OrderMapper {
    /**
     * 根据id查询订单详情
     *
     * @param id 订单id
     * @return 订单详情
     */
    Order selectById(@Param("id") Long id);

    /**
     * 根据用户id，查询用户的所有订单
     *
     * @param userId 用户id
     * @return 订单列表
     */
    List<Order> selectListByUserId(@Param("userId") Long userId);

    /**
     * 插入一条订单数据
     *
     * @param order 订单
     */
    void insert(Order order);
}
