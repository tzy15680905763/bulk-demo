package top.bulk.db.sharding.entity;

import lombok.Data;

/**
 * 用户类
 *
 * @author 散装java
 * @date 2023-08-29
 */
@Data
public class User {
    private Long id;
    private String username;
    private String phone;
}
