package top.bulk.db.sharding.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 订单明细表
 *
 * @author 散装java
 * @date 2023-08-29
 */
@Data
public class OrderItem {
    private Long id;
    private Long orderId;
    private Long userId;
    private String productName;
    private LocalDateTime dateTime;
}
