package top.bulk.db.sharding.mapper;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import top.bulk.db.sharding.entity.Order;

import javax.annotation.Resource;

/**
 * 订单mapper测试类
 *
 * @author 散装java
 * @date 2023-08-29
 */
@SpringBootTest
@Slf4j
class OrderMapperTest {
    @Resource
    OrderMapper orderMapper;

    @Test
    void selectById() {
    }

    @Test
    void selectListByUserId() {
    }

    @Test
    void insert() {
        for (int i = 0; i < 10; i++) {
            Order order = new Order();
            order.setUserId(0L);
            orderMapper.insert(order);
        }


        log.info("插入测试成功");
    }

    @Test
    void testGet() {
        // 这个id，是位于 库ds-0的t_order_0表中
        long id = 903939342441906176L;
        Order order = orderMapper.selectById(id);
        System.out.println(order);
    }
}