# Spring Boot 整合 sharding-jdbc 分库分表

[官方github](https://github.com/apache/shardingsphere)

[官方文档(中文)](https://shardingsphere.apache.org/document/current/cn/overview/)

### 什么是 ShardingSphere

Apache ShardingSphere 是一款分布式的数据库生态系统， 可以将任意数据库转换为分布式数据库，并通过数据分片、弹性伸缩、加密等能力对原有数据库进行增强。

Apache ShardingSphere 设计哲学为 Database Plus，旨在构建异构数据库上层的标准和生态。 它关注如何充分合理地利用数据库的计算和存储能力，而并非实现一个全新的数据库。
它站在数据库的上层视角，关注它们之间的协作多于数据库自身。

### ShardingSphere-JDBC

ShardingSphere-JDBC 定位为轻量级 Java 框架，在 Java 的 JDBC 层提供的额外服务。

## 演示

本示例的 数据库表分布如下

![](./img/db-show.png)

### 测试
