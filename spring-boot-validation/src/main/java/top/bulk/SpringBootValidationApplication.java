package top.bulk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 参数校验 + 全局异常拦截 演示
 *
 * @author 散装java
 * @version 1.0.0
 * @date 2024-04-07
 */
@SpringBootApplication
// 表示通过aop框架暴露该代理对象,AopContext能够访问
@EnableAspectJAutoProxy(exposeProxy = true)
public class SpringBootValidationApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootValidationApplication.class);
    }
}