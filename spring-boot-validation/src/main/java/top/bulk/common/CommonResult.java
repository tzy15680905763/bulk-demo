package top.bulk.common;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 通用返回对象
 *
 * @author 散装java
 * @version 1.0.0
 * @date 2024-06-09
 */
@Data
@AllArgsConstructor
public class CommonResult<T> {
    private long code;

    private String message;

    private T data;

    /**
     * 成功返回结果
     *
     * @param data 获取的数据
     */
    public static <T> CommonResult<T> success(T data) {
        return new CommonResult<T>(200, "操作成功", data);
    }

    /**
     * 失败返回结果
     *
     * @param message 提示信息
     */
    public static <T> CommonResult<T> err(String message) {
        return new CommonResult<T>(500, message, null);
    }

    public static <T> CommonResult<T> err(Integer code, String message) {
        return new CommonResult<T>(code, message, null);
    }
}
