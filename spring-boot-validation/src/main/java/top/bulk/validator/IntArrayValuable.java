package top.bulk.validator;

/**
 * 可写成 int 数组的统一接口
 *
 * @author 散装java
 * @version 1.0.0
 * @date 2024-06-10
 */
public interface IntArrayValuable  {

    /**
     * @return 返回一个int类型的数据
     */
    int[] array();

}
