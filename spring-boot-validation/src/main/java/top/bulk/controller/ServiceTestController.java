package top.bulk.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.bulk.entity.UserEntity;
import top.bulk.service.UserService;

import javax.annotation.Resource;

/**
 * 非 controller 层校验测试
 *
 * @author 散装java
 * @version 1.0.0
 * @date 2024-07-27
 */
@RestController
@RequestMapping("/serviceTest")
public class ServiceTestController {
    @Resource
    private UserService userService;

    @RequestMapping("/get")
    public String get() {
        userService.get(0);
        return "success";
    }


    @RequestMapping("/add")
    public String add() {
        userService.add(new UserEntity());
        return "success";
    }

    @RequestMapping("/add2")
    public String add2() {
        userService.add2(new UserEntity());
        return "success";
    }

}
