package top.bulk.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 业务异常枚举
 */
@AllArgsConstructor
@Getter
public enum ServiceExceptionEnum {
    ERROR(1,"未知异常"),
    MISSING_REQUEST_PARAM_ERROR(500, "参数缺失"),
    INVALID_REQUEST_PARAM_ERROR(500, "请求参数不合法"),
    ;

    /**
     * 错误码
     */
    private final int code;
    /**
     * 错误提示
     */
    private final String message;
}
