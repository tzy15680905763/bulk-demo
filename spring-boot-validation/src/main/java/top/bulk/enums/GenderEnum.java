package top.bulk.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import top.bulk.validator.IntArrayValuable;

import java.util.Arrays;

/**
 * 性别的枚举类
 * 要实现 IntArrayValuable 类，因为在自定义注解中会使用
 *
 * @author 散装java
 * @version 1.0.0
 * @date 2024-06-10
 */
@AllArgsConstructor
@Getter
public enum GenderEnum implements IntArrayValuable {

    OTHER(0, "其他"), MALE(1, "男"), FEMALE(2, "女");

    /**
     * 值数组
     */
    public static final int[] ARRAYS = Arrays.stream(GenderEnum.values()).mapToInt(GenderEnum::getCode).toArray();

    /**
     * 性别值
     */
    private final Integer code;
    /**
     * 性别名
     */
    private final String name;

    @Override
    public int[] array() {
        return ARRAYS;
    }
}
