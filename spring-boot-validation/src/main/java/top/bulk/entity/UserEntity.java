package top.bulk.entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import top.bulk.enums.GenderEnum;
import top.bulk.validator.InEnum;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * 用户实体类
 *
 * @author 散装java
 * @version 1.0.0
 * @date 2024-04-07
 */
@Data
public class UserEntity {
    @Min(value = 100)
    private Integer id;

    @NotEmpty(message = "用户名不能为空")
    @Length(min = 5, max = 16, message = "用户名长度为 5-16 位")
    @Pattern(regexp = "^[A-Za-z0-9]+$", message = "用户名格式为数字以及字母")
    private String username;


    @NotNull(message = "性别不能为空")
    @InEnum(value = GenderEnum.class, message = "性别必须是 {value}")
    private Integer gender;
    /**
     * 这里要想校验 address 中的内容，只能用 @Valid
     */
    @Valid
    @NotNull(message = "地址不能为空")
    private Address address;


}
