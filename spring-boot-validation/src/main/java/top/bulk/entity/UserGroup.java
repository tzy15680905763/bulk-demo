package top.bulk.entity;

import lombok.Data;
import top.bulk.enums.GenderEnum;
import top.bulk.validator.InEnum;
import top.bulk.validator.group.AddGroup;
import top.bulk.validator.group.UpdateGroup;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 用户实体类
 *
 * @author 散装java
 * @version 1.0.0
 * @date 2024-04-07
 */
@Data
public class UserGroup {

    /**
     * 设置id在 更新组中校验生效
     */
    @NotNull(message = "id不能为空", groups = UpdateGroup.class)
    private Integer id;

    /**
     * 设置用户名在 添加组中校验生效
     */
    @NotEmpty(message = "用户名不能为空", groups = AddGroup.class)
    private String username;

    @InEnum(value = GenderEnum.class, groups = AddGroup.class, message = "性别必须是 {value}")
    private Integer gender;
}
