package top.bulk.entity;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author 散装java
 * @version 1.0.0
 * @date 2024-06-10
 */
@Data
public class Address {
    @NotBlank(message = "省份不能为空")
    private String province;
    private String city;
    private String district;
}
