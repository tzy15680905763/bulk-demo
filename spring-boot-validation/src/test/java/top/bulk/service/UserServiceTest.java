package top.bulk.service;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import top.bulk.entity.UserEntity;

import javax.annotation.Resource;

/**
 * @author 散装java
 * @version 1.0.0
 * @date 2024-06-10
 */
@SpringBootTest
class UserServiceTest {
    @Resource
    UserService userService;

    /**
     * 测试直接调用service，注释是否生效
     */
    @Test
    void get() {
        String res = userService.get(0);
        // 抛出异常 javax.validation.ConstraintViolationException: get.id: 编号必须大于 0
    }

    /**
     * 测试直接调用service，注释是否生效
     * 测试嵌套校验
     */
    @Test
    void add() {
        UserEntity user = new UserEntity();
        user.setUsername("bulkJava");
        userService.add(user);
        // 抛出异常 javax.validation.ConstraintViolationException: get.id: 编号必须大于 0
    }

    @Test
    void add2() {
        UserEntity user = new UserEntity();
        user.setUsername("bulkJava");
        userService.add2(user);
    }
}