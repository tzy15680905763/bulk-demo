# Spring Boot 整合  Hibernate-Validator 实现优雅的数据校验

> spring-boot-starter-web 默认内置了Hibernate-Validator（Spring boot 2.3以前版本）
>
> 本示例，是基于2.7.x的版本，所以需要自己引入spring-boot-starter-validation 来提供支持
>
> spring-boot-starter-validation 不仅支持 JSR-303（Bean Validation 1.0）规范，还提供了对 JSR-380（Bean Validation
> 2.0）规范的全面支持。这使得开发者可以利用 Bean Validation 2.0 的新特性，更灵活地定义验证规则，包括对集合、嵌套对象的验证等。


常见的校验注解的含义

### Bean Validation 定义的约束注解

1. 空和非空检查
    - @NotBlank ：只能用于字符串不为 null ，并且字符串 #trim() 以后 length 要大于 0 。
    - @NotEmpty ：集合对象的元素不为 0 ，即集合不为空，也可以用于字符串不为 null 。
    - @NotNull ：不能为 null 。
    - @Null ：必须为 null 。
2. 数值检查
    - @DecimalMax(value) ：被注释的元素必须是一个数字，其值必须小于等于指定的最大值。
    - @DecimalMin(value) ：被注释的元素必须是一个数字，其值必须大于等于指定的最小值。
    - @Digits(integer, fraction) ：被注释的元素必须是一个数字，其值必须在可接受的范围内。
    - @Positive ：判断正数。
    - @PositiveOrZero ：判断正数或 0 。
    - @Max(value) ：该字段的值只能小于或等于该值。
    - @Min(value) ：该字段的值只能大于或等于该值。
    - @Negative ：判断负数。
    - @NegativeOrZero ：判断负数或 0 。
3. Boolean 值检查
    - @AssertFalse ：被注释的元素必须为 true 。
    - @AssertTrue ：被注释的元素必须为 false 。
4. 长度检查
    - @Size(max, min) ：检查该字段的 size 是否在 min 和 max 之间，可以是字符串、数组、集合、Map 等。
5. 日期检查
    - @Future ：被注释的元素必须是一个将来的日期。
    - @FutureOrPresent ：判断日期是否是将来或现在日期。
    - @Past ：检查该字段的日期是在过去。
    - @PastOrPresent ：判断日期是否是过去或现在日期。
6. 邮箱检查
    - @Email ：被注释的元素必须是电子邮箱地址。
7. 正则检查
    - @Pattern(value) ：被注释的元素必须符合指定的正则表达式。

### org.hibernate.validator.constraints 包下，定义了一系列的约束( constraint )注解

- @Range(min=, max=) ：被注释的元素必须在合适的范围内。
- @Length(min=, max=) ：被注释的字符串的大小必须在指定的范围内。
- @URL(protocol=,host=,port=,regexp=,flags=) ：被注释的字符串必须是一个有效的 URL 。
- @SafeHtml ：判断提交的 HTML 是否安全。例如说，不能包含 javascript 脚本等等。
- 等等

### @Valid 和 @Validated

- @Valid 注解，是 Bean Validation 所定义，可以添加在普通方法、构造方法、方法参数、方法返回、成员变量上，表示它们需要进行约束校验。
  **可以用于嵌套校验**

- @Validated 注解，是 Spring Validation 锁定义，可以添加在类、方法参数、普通方法上，表示它们需要进行约束校验。同时，@Validated
  有 value 属性，**支持分组校验**

### 嵌套校验

参考，top.bulk.entity.UserEntity.address 的校验

### 分组校验
参考 top.bulk.entity.UserGroup 类使用的到的地方

### 自定义校验注解

参考 top.bulk.validator 包下的内容，定义了一个校验是否在枚举内的注解

## 测试

本 demo 的测试，参考 

http 接口测试：`spring-boot-validation/doc/test-requests.http`

service 接口测试：`spring-boot-validation/src/test/java/top/bulk/service/UserServiceTest.java` 
