package top.bulk.unit.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author 散装java
 * @version 1.0.0
 * @date 2024-06-11
 */
@Data
// 支持链式调用
@Accessors(chain = true)
public class UserEntity {
    private Integer id;
    private String username;
    private String password;
}
