package top.bulk.unit.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.bulk.unit.entity.UserEntity;
import top.bulk.unit.service.UserService;

import javax.annotation.Resource;

/**
 * @author 散装java
 * @version 1.0.0
 * @date 2024-06-11
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
    @Resource
    private UserService userService;

    @GetMapping("/get")
    public UserEntity get(@RequestParam("id") Integer id) {
        UserEntity user = userService.get(id);
        log.info("UserController.get({}),res:{}", id, user);
        return user;
    }
}
