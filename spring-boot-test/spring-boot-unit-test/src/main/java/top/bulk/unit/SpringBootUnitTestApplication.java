package top.bulk.unit;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 散装java
 * @version 1.0.0
 * @date 2024-06-11
 */
@SpringBootApplication
@MapperScan(basePackages = "top.bulk.unit.mapper")
public class SpringBootUnitTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootUnitTestApplication.class);
    }
}
