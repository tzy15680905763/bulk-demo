package top.bulk.unit.mapper;

import org.apache.ibatis.annotations.Select;
import top.bulk.unit.entity.UserEntity;

/**
 * @author 散装java
 * @version 1.0.0
 * @date 2024-06-11
 */
public interface UserMapper {
    @Select("select * from t_user where id = #{id}")
    UserEntity selectById(Integer id);
}
