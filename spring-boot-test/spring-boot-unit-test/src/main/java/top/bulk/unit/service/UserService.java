package top.bulk.unit.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.bulk.unit.entity.UserEntity;
import top.bulk.unit.mapper.UserMapper;

import javax.annotation.Resource;

/**
 * @author 散装java
 * @version 1.0.0
 * @date 2024-06-11
 */
@Service
@Slf4j
public class UserService {
    @Resource
    private UserMapper userMapper;

    public UserEntity get(Integer id) {
        log.info("UserService.get:{}", id);
        return userMapper.selectById(id);
    }
}
