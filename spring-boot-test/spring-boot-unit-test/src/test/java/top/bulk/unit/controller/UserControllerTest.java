package top.bulk.unit.controller;

import lombok.SneakyThrows;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import top.bulk.unit.entity.UserEntity;
import top.bulk.unit.service.UserService;

/**
 * 使用代理的 UserService ，测试 UserController
 *
 * @author 散装java
 * @version 1.0.0
 * @date 2024-06-11
 */
// @ExtendWith(SpringExtension.class)
// @WebMvcTest(UserController.class)

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {
    /**
     * 配合 @AutoConfigureMockMvc 调用后端 API 接口
     */
    @Autowired
    private MockMvc mvc;

    /**
     * 创建了一个基于 Mockito 的 UserService Mock 代理对象 Bean
     * 同时，该 Bean 会注入到依赖 UserService 的 UserController 中
     */
    @MockBean
    private UserService userService;
    @SneakyThrows
    @Test
    void get() {
        UserEntity user = new UserEntity().setId(1).setUsername("bulk").setPassword("java");
        // Mock UserService 的 get 方法
        // 当调用 userService.get(1) 方法时，返回一个 UserEntity 对象
        Mockito.when(userService.get(1)).thenReturn(user);

        // 这里会调用 /user/get?id=1 接口，就是 top.bulk.unit.controller.UserController.get 的方法
        // 但是这里的 userService 并不是 UserController 类中原本的service ，而且被替换为 mock 的service
        // 注意，这里只设置了代理 get(1),如果你请求get(2)就查不到数据了
        ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.get("/user/get?id=1"));

        // 校验响应状态码是否为 200
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());

        // 打印请求响应的对象体
        resultActions.andDo(result -> System.out.println("请求响应:" + result.getResponse().getContentAsString()));

        // 校验相应结果，校验响应内容方式一：直接全部匹配
        String expect = "{\"id\":1,\"username\":\"bulk\",\"password\":\"java\"}";
        resultActions.andExpect(MockMvcResultMatchers.content().json(expect, true)); // 响应结果

        // 校验响应内容方式二：逐个字段匹配
        resultActions.andExpect(MockMvcResultMatchers.jsonPath("id", IsEqual.equalTo(1)));
        resultActions.andExpect(MockMvcResultMatchers.jsonPath("username", IsEqual.equalTo("bulk")));
        resultActions.andExpect(MockMvcResultMatchers.jsonPath("password", IsEqual.equalTo("java")));
    }

    /**
     * `src/test/resources/application.yaml` 文件会替换你项目里的的配置文件
     * <p>
     * 挺坑的，如果你新建了，那么他默认走的就是h2的数据库，那么怕你把 `src/test/resources/application.yaml` 中的配置全都注释了
     * 因为他默认就是 h2
     */
    @Test
    void getNotMock() throws Exception {
        ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.get("/user/get?id=1"));
        resultActions.andDo(result -> System.out.println("请求响应:" + result.getResponse().getContentAsString()));
    }
}