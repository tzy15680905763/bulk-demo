package top.bulk.unit.mapper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import top.bulk.unit.entity.UserEntity;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author 散装java
 * @date 2024-06-12
 */
@SpringBootTest
class UserMapperTest {
    @Resource
    private UserMapper userMapper;

    /**
     * 基于 内存数据库 h2 进行数据操作，其中三个 `@Sql` 的作用为
     * 1. 在单元测试方法执行之前，执行 /sql/create_tables.sql 脚本，创建 t_user 表
     * 2. 在单元测试方法执行之前，执行在 statements 属性定义的 SQL 操作，插入一条 id=1 的数据
     * 3. 在单元测试方法执行之后，/sql/clean.sql 脚本，清空数据
     * <p>
     * sql的位置为 src/test/resources/sql/create_tables.sql
     */
    @Test
    @Sql(scripts = "/sql/create_tables.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "INSERT INTO `t_user`(`id`, `username`, `password`) VALUES (1, 'bulk', 'java');", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "/sql/clean.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void selectById() {
        // 调用 userMapper 查询你用户，这里查到的并不是 MySQL中的数据，而是 @Sql 注解插入的数据
        UserEntity user = userMapper.selectById(1);

        System.out.println(user);
        // 使用 JUnit 提供的 断言 校验结果
        Assertions.assertEquals(1, user.getId(), "编号不匹配");
        Assertions.assertEquals("bulk", user.getUsername(), "用户名不匹配");
        Assertions.assertEquals("java", user.getPassword(), "密码不匹配");
    }
}