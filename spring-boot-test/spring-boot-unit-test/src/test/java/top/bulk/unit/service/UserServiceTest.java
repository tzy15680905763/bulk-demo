package top.bulk.unit.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import top.bulk.unit.entity.UserEntity;
import top.bulk.unit.mapper.UserMapper;

/**
 * UserService 测试类
 *
 * @author 散装java
 * @date 2024-06-12
 */
@SpringBootTest
class UserServiceTest {
    /**
     * `@MockBean` 对 UserMapper 进行 mock。
     */
    @MockBean
    private UserMapper userMapper;

    @Autowired
    private UserService userService;

    @Test
    void get() {
        UserEntity user = new UserEntity().setId(1).setUsername("bulk").setPassword("java");

        // Mock UserMapper 的 selectById 方法，即。当调用userMapper.selectById(1) 时候，返回 上面 new 的user
        Mockito.when(userMapper.selectById(1)).thenReturn(user);

        // 调用 service
        UserEntity entity = userService.get(1);
        System.out.println("查询结果：" + entity);

        // 使用 JUnit 提供的 断言 校验结果
        Assertions.assertEquals(1, entity.getId(), "编号不匹配");
        Assertions.assertEquals("bulk", entity.getUsername(), "用户名不匹配");
        Assertions.assertEquals("java", entity.getPassword(), "密码不匹配");
    }
}