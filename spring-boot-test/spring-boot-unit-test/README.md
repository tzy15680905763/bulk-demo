# Spring Boot 集成单元测试 spring-boot-starter-test

本示例会演示单元测试中的 mock 操作

### controller mock 的测试

参考类 `top.bulk.unit.controller.UserControllerTest`

实现通过 rest 调用 controller 时候，mock 其中的 service 从而实现测试效果

### service mock 的测试

参考 `top.bulk.unit.service.UserServiceTest`

### 数据库层面的模拟测试

参考 `top.bulk.unit.mapper.UserMapperTest`

### 注意

在跑测试的时候

`src/test/resources/application.yaml` 文件会替换你项目里的的配置文件

挺坑的，如果你新建了，那么他默认走的就是h2的数据库，那么怕你把 `src/test/resources/application.yaml` 中的配置全都注释了
因为他默认就是 h2