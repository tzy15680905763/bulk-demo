# Spring Boot 下，对 IDEA 自带的 http client 做演示

> IDEA HTTP Client 是 IDEA 提供的 HTTP API 测试插件，默认内置，直接开启即可使用
>
> 官方文档，参考 https://www.jetbrains.com/help/idea/http-client-in-product-code-editor.html

先说一下，使用这个做 http 测试的好处

1. 不需要额外安装软件，只要你使用 idea 即可
2. 其采用 .http 的文本文件，可以放到项目中，受 git 管理，从而实现团队协作的共享。
3. idea 对其有良好的编写提示，支持 Live Template ，以及可以做自动生成
4. 支持环境变量配置，方便切换参数

![](./img/generation.png)

内置的模版支持
![](./img/live-templates.png)

### 1.基本使用演示

参考 `http/UserController.http`

### 2.环境变量 使用演示

1. http-client.env.json 配置文件，定义通用变量。例如说，url 地址、port 端口等等。
2. http-client.private.env.json 配置文件，定义敏感变量。例如说，username/password 账号密码、token 访问令牌等等

参考 `http/UserControllerWithEnv.http`

### 3.断言 使用演示

参考 `http/UserControllerWithAssert.http`

![](./img/assert-show.png)

### 4.内存暂存结果 使用演示

参考 `http/UserControllerWithStaging.http`