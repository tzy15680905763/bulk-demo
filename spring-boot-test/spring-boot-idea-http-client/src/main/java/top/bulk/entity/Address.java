package top.bulk.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author 散装java
 * @date 2024-06-12
 */
@Data
@Accessors(chain = true)
public class Address {
    private String province;
    private String city;
    private String district;
}
