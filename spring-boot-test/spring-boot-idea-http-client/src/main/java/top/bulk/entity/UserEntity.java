package top.bulk.entity;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 实体类
 *
 * @author 散装java
 * @date 2024-06-12
 */
@Data
@ToString
@Accessors(chain = true)
public class UserEntity {
    private Integer id;
    private String username;
    private String password;
    /**
     * 方便测试生成嵌套实体类的操作
     */
    private Address address;
}
