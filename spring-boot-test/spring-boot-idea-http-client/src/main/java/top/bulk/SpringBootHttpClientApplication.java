package top.bulk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 散装java
 * @version 1.0.0
 * @date 2024-06-11
 */
@SpringBootApplication
public class SpringBootHttpClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootHttpClientApplication.class);
    }
}
