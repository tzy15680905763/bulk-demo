package top.bulk.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import top.bulk.entity.Address;
import top.bulk.entity.UserEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 散装java
 * @date 2024-06-12
 */
@RestController
@Slf4j
public class UserController {
    private final UserEntity user = new UserEntity().setId(123).setUsername("bulkJava").setAddress(new Address().setProvince("江苏"));
    private final String token = "token123123";

    /**
     * POST 请求 + Form 的示例
     */
    @PostMapping("/user/login")
    public Map<String, Object> login(@RequestParam("username") String username,
                                     @RequestParam("password") String password) {
        log.info("username:{},password:{}", username, password);
        HashMap<String, Object> map = new HashMap<>();
        if ("bulkJava".equals(username) && "123456".equals(password)) {
            map.put("msg", "登录成功");
            map.put("user", user);
            map.put("token", token);
        } else {
            map.put("msg", "登录失败");
        }
        return map;

    }

    /**
     * 演示 json 类型的请求
     */
    @PostMapping("/user/add")
    public Boolean addUser(@RequestBody UserEntity user) {
        log.info("addUser:{}", user);
        return true;
    }

    /**
     * 演示请求头获取参数
     */
    @GetMapping("/user/get-current")
    public UserEntity getCurrent(@RequestHeader("Authorization") String authorization) {
        if (token.equals(authorization)) {
            return user;
        }
        throw new RuntimeException("请登录！");
    }
}
